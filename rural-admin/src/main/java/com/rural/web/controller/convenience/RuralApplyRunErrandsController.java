package com.rural.web.controller.convenience;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralApplyRunErrands;
import com.rural.system.service.IRuralApplyRunErrandsService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 申请跑腿Controller
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
@RestController
@RequestMapping("/convenience/errands")
public class RuralApplyRunErrandsController extends BaseController
{
    @Autowired
    private IRuralApplyRunErrandsService ruralApplyRunErrandsService;

    /**
     * 查询申请跑腿列表
     */
    @PreAuthorize("@ss.hasPermi('convenience:errands:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralApplyRunErrands ruralApplyRunErrands)
    {
        startPage();
        List<RuralApplyRunErrands> list = ruralApplyRunErrandsService.selectRuralApplyRunErrandsList(ruralApplyRunErrands);
        return getDataTable(list);
    }

    /**
     * 导出申请跑腿列表
     */
    @PreAuthorize("@ss.hasPermi('convenience:errands:export')")
    @Log(title = "申请跑腿", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralApplyRunErrands ruralApplyRunErrands)
    {
        List<RuralApplyRunErrands> list = ruralApplyRunErrandsService.selectRuralApplyRunErrandsList(ruralApplyRunErrands);
        ExcelUtil<RuralApplyRunErrands> util = new ExcelUtil<RuralApplyRunErrands>(RuralApplyRunErrands.class);
        util.exportExcel(response, list, "申请跑腿数据");
    }

    /**
     * 获取申请跑腿详细信息
     */
    @PreAuthorize("@ss.hasPermi('convenience:errands:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralApplyRunErrandsService.selectRuralApplyRunErrandsByRId(rId));
    }

    /**
     * 新增申请跑腿
     */
    @PreAuthorize("@ss.hasPermi('convenience:errands:add')")
    @Log(title = "申请跑腿", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralApplyRunErrands ruralApplyRunErrands)
    {
        return toAjax(ruralApplyRunErrandsService.insertRuralApplyRunErrands(ruralApplyRunErrands));
    }

    /**
     * 修改申请跑腿
     */
    @PreAuthorize("@ss.hasPermi('convenience:errands:edit')")
    @Log(title = "申请跑腿", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralApplyRunErrands ruralApplyRunErrands)
    {
        return toAjax(ruralApplyRunErrandsService.updateRuralApplyRunErrands(ruralApplyRunErrands));
    }

    /**
     * 删除申请跑腿
     */
    @PreAuthorize("@ss.hasPermi('convenience:errands:remove')")
    @Log(title = "申请跑腿", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralApplyRunErrandsService.deleteRuralApplyRunErrandsByRIds(rIds));
    }
}
