package com.rural.web.controller.ruralpcm;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralProposal;
import com.rural.system.service.IRuralProposalService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 我有建议Controller
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
@RestController
@RequestMapping("/ruralpcm/proposal")
public class RuralProposalController extends BaseController
{
    @Autowired
    private IRuralProposalService ruralProposalService;

    /**
     * 查询我有建议列表
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:proposal:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralProposal ruralProposal)
    {
        startPage();
        List<RuralProposal> list = ruralProposalService.selectRuralProposalList(ruralProposal);
        return getDataTable(list);
    }

    /**
     * 导出我有建议列表
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:proposal:export')")
    @Log(title = "我有建议", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralProposal ruralProposal)
    {
        List<RuralProposal> list = ruralProposalService.selectRuralProposalList(ruralProposal);
        ExcelUtil<RuralProposal> util = new ExcelUtil<RuralProposal>(RuralProposal.class);
        util.exportExcel(response, list, "我有建议数据");
    }

    /**
     * 获取我有建议详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:proposal:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralProposalService.selectRuralProposalByRId(rId));
    }

    /**
     * 新增我有建议
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:proposal:add')")
    @Log(title = "我有建议", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralProposal ruralProposal)
    {
        return toAjax(ruralProposalService.insertRuralProposal(ruralProposal));
    }

    /**
     * 修改我有建议
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:proposal:edit')")
    @Log(title = "我有建议", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralProposal ruralProposal)
    {
        return toAjax(ruralProposalService.updateRuralProposal(ruralProposal));
    }

    /**
     * 删除我有建议
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:proposal:remove')")
    @Log(title = "我有建议", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralProposalService.deleteRuralProposalByRIds(rIds));
    }
}
