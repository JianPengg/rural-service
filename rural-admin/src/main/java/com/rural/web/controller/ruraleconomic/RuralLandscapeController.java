package com.rural.web.controller.ruraleconomic;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralLandscape;
import com.rural.system.service.IRuralLandscapeService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 乡村景观Controller
 *
 * @author ruoyi
 * @date 2022-05-10
 */
@RestController
@RequestMapping("/ruraleconomic/landscape")
public class RuralLandscapeController extends BaseController
{
    @Autowired
    private IRuralLandscapeService ruralLandscapeService;

    /**
     * 查询乡村景观列表
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:landscape:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralLandscape ruralLandscape)
    {
        startPage();
        List<RuralLandscape> list = ruralLandscapeService.selectRuralLandscapeList(ruralLandscape);
        return getDataTable(list);
    }

    /**
     * 导出乡村景观列表
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:landscape:export')")
    @Log(title = "乡村景观", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralLandscape ruralLandscape)
    {
        List<RuralLandscape> list = ruralLandscapeService.selectRuralLandscapeList(ruralLandscape);
        ExcelUtil<RuralLandscape> util = new ExcelUtil<RuralLandscape>(RuralLandscape.class);
        util.exportExcel(response, list, "乡村景观数据");
    }

    /**
     * 获取乡村景观详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:landscape:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralLandscapeService.selectRuralLandscapeByRId(rId));
    }

    /**
     * 新增乡村景观
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:landscape:add')")
    @Log(title = "乡村景观", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralLandscape ruralLandscape)
    {
        return toAjax(ruralLandscapeService.insertRuralLandscape(ruralLandscape));
    }

    /**
     * 修改乡村景观
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:landscape:edit')")
    @Log(title = "乡村景观", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralLandscape ruralLandscape)
    {
        return toAjax(ruralLandscapeService.updateRuralLandscape(ruralLandscape));
    }

    /**
     * 删除乡村景观
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:landscape:remove')")
    @Log(title = "乡村景观", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralLandscapeService.deleteRuralLandscapeByRIds(rIds));
    }
}
