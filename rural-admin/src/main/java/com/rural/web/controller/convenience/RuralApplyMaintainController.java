package com.rural.web.controller.convenience;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralApplyMaintain;
import com.rural.system.service.IRuralApplyMaintainService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 申请维修Controller
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
@RestController
@RequestMapping("/convenience/maintain")
public class RuralApplyMaintainController extends BaseController
{
    @Autowired
    private IRuralApplyMaintainService ruralApplyMaintainService;

    /**
     * 查询申请维修列表
     */
    @PreAuthorize("@ss.hasPermi('convenience:maintain:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralApplyMaintain ruralApplyMaintain)
    {
        startPage();
        List<RuralApplyMaintain> list = ruralApplyMaintainService.selectRuralApplyMaintainList(ruralApplyMaintain);
        return getDataTable(list);
    }

    /**
     * 导出申请维修列表
     */
    @PreAuthorize("@ss.hasPermi('convenience:maintain:export')")
    @Log(title = "申请维修", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralApplyMaintain ruralApplyMaintain)
    {
        List<RuralApplyMaintain> list = ruralApplyMaintainService.selectRuralApplyMaintainList(ruralApplyMaintain);
        ExcelUtil<RuralApplyMaintain> util = new ExcelUtil<RuralApplyMaintain>(RuralApplyMaintain.class);
        util.exportExcel(response, list, "申请维修数据");
    }

    /**
     * 获取申请维修详细信息
     */
    @PreAuthorize("@ss.hasPermi('convenience:maintain:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralApplyMaintainService.selectRuralApplyMaintainByRId(rId));
    }

    /**
     * 新增申请维修
     */
    @PreAuthorize("@ss.hasPermi('convenience:maintain:add')")
    @Log(title = "申请维修", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralApplyMaintain ruralApplyMaintain)
    {
        return toAjax(ruralApplyMaintainService.insertRuralApplyMaintain(ruralApplyMaintain));
    }

    /**
     * 修改申请维修
     */
    @PreAuthorize("@ss.hasPermi('convenience:maintain:edit')")
    @Log(title = "申请维修", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralApplyMaintain ruralApplyMaintain)
    {
        return toAjax(ruralApplyMaintainService.updateRuralApplyMaintain(ruralApplyMaintain));
    }

    /**
     * 删除申请维修
     */
    @PreAuthorize("@ss.hasPermi('convenience:maintain:remove')")
    @Log(title = "申请维修", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralApplyMaintainService.deleteRuralApplyMaintainByRIds(rIds));
    }
}
