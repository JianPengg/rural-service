package com.rural.web.controller.convenience;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralApplyInfrastructure;
import com.rural.system.service.IRuralApplyInfrastructureService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 申请基础设施Controller
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
@RestController
@RequestMapping("/convenience/infrastructure")
public class RuralApplyInfrastructureController extends BaseController
{
    @Autowired
    private IRuralApplyInfrastructureService ruralApplyInfrastructureService;

    /**
     * 查询申请基础设施列表
     */
    @PreAuthorize("@ss.hasPermi('convenience:infrastructure:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        startPage();
        List<RuralApplyInfrastructure> list = ruralApplyInfrastructureService.selectRuralApplyInfrastructureList(ruralApplyInfrastructure);
        return getDataTable(list);
    }

    /**
     * 导出申请基础设施列表
     */
    @PreAuthorize("@ss.hasPermi('convenience:infrastructure:export')")
    @Log(title = "申请基础设施", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        List<RuralApplyInfrastructure> list = ruralApplyInfrastructureService.selectRuralApplyInfrastructureList(ruralApplyInfrastructure);
        ExcelUtil<RuralApplyInfrastructure> util = new ExcelUtil<RuralApplyInfrastructure>(RuralApplyInfrastructure.class);
        util.exportExcel(response, list, "申请基础设施数据");
    }

    /**
     * 获取申请基础设施详细信息
     */
    @PreAuthorize("@ss.hasPermi('convenience:infrastructure:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralApplyInfrastructureService.selectRuralApplyInfrastructureByRId(rId));
    }

    /**
     * 新增申请基础设施
     */
    @PreAuthorize("@ss.hasPermi('convenience:infrastructure:add')")
    @Log(title = "申请基础设施", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        return toAjax(ruralApplyInfrastructureService.insertRuralApplyInfrastructure(ruralApplyInfrastructure));
    }

    /**
     * 修改申请基础设施
     */
    @PreAuthorize("@ss.hasPermi('convenience:infrastructure:edit')")
    @Log(title = "申请基础设施", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        return toAjax(ruralApplyInfrastructureService.updateRuralApplyInfrastructure(ruralApplyInfrastructure));
    }

    /**
     * 删除申请基础设施
     */
    @PreAuthorize("@ss.hasPermi('convenience:infrastructure:remove')")
    @Log(title = "申请基础设施", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralApplyInfrastructureService.deleteRuralApplyInfrastructureByRIds(rIds));
    }
}
