package com.rural.web.controller.ruralpcm;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralComplaint;
import com.rural.system.service.IRuralComplaintService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 我要投诉Controller
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/ruralpcm/complaint")
public class RuralComplaintController extends BaseController
{
    @Autowired
    private IRuralComplaintService ruralComplaintService;

    /**
     * 查询我要投诉列表
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:complaint:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralComplaint ruralComplaint)
    {
        startPage();
        List<RuralComplaint> list = ruralComplaintService.selectRuralComplaintList(ruralComplaint);
        return getDataTable(list);
    }

    /**
     * 导出我要投诉列表
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:complaint:export')")
    @Log(title = "我要投诉", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralComplaint ruralComplaint)
    {
        List<RuralComplaint> list = ruralComplaintService.selectRuralComplaintList(ruralComplaint);
        ExcelUtil<RuralComplaint> util = new ExcelUtil<RuralComplaint>(RuralComplaint.class);
        util.exportExcel(response, list, "我要投诉数据");
    }

    /**
     * 获取我要投诉详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:complaint:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralComplaintService.selectRuralComplaintByRId(rId));
    }

    /**
     * 新增我要投诉
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:complaint:add')")
    @Log(title = "我要投诉", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralComplaint ruralComplaint)
    {
        return toAjax(ruralComplaintService.insertRuralComplaint(ruralComplaint));
    }

    /**
     * 修改我要投诉
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:complaint:edit')")
    @Log(title = "我要投诉", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralComplaint ruralComplaint)
    {
        return toAjax(ruralComplaintService.updateRuralComplaint(ruralComplaint));
    }

    /**
     * 删除我要投诉
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:complaint:remove')")
    @Log(title = "我要投诉", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralComplaintService.deleteRuralComplaintByRIds(rIds));
    }
}
