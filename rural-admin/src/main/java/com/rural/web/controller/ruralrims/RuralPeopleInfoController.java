package com.rural.web.controller.ruralrims;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralPeopleInfo;
import com.rural.system.service.IRuralPeopleInfoService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 人口信息Controller
 * 
 * @author ruoyi
 * @date 2022-05-11
 */
@RestController
@RequestMapping("/ruralrims/peopleinfo")
public class RuralPeopleInfoController extends BaseController
{
    @Autowired
    private IRuralPeopleInfoService ruralPeopleInfoService;


    @GetMapping("/list")
    public TableDataInfo list(RuralPeopleInfo ruralPeopleInfo)
    {
        startPage();
        List<RuralPeopleInfo> list = ruralPeopleInfoService.selectRuralPeopleInfoList(ruralPeopleInfo);
        return getDataTable(list);
    }


    /**
     * 查询人口男女比例,小孩,成年,老人比例
     */
    @GetMapping("/getPeopleRatio")
    public AjaxResult getPeopleRatio(RuralPeopleInfo ruralPeopleInfo)
    {
        return AjaxResult.success(ruralPeopleInfoService.getPeopleRatio(ruralPeopleInfo));
    }

    /**
     * 导出人口信息列表
     */
    @PreAuthorize("@ss.hasPermi('ruralrims:info:export')")
    @Log(title = "人口信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralPeopleInfo ruralPeopleInfo)
    {
        List<RuralPeopleInfo> list = ruralPeopleInfoService.selectRuralPeopleInfoList(ruralPeopleInfo);
        ExcelUtil<RuralPeopleInfo> util = new ExcelUtil<RuralPeopleInfo>(RuralPeopleInfo.class);
        util.exportExcel(response, list, "人口信息数据");
    }

    /**
     * 获取人口信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruralrims:info:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralPeopleInfoService.selectRuralPeopleInfoByRId(rId));
    }

    /**
     * 新增人口信息
     */
    @PreAuthorize("@ss.hasPermi('ruralrims:info:add')")
    @Log(title = "人口信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralPeopleInfo ruralPeopleInfo)
    {
        return toAjax(ruralPeopleInfoService.insertRuralPeopleInfo(ruralPeopleInfo));
    }

    /**
     * 修改人口信息
     */
    @PreAuthorize("@ss.hasPermi('ruralrims:info:edit')")
    @Log(title = "人口信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralPeopleInfo ruralPeopleInfo)
    {
        return toAjax(ruralPeopleInfoService.updateRuralPeopleInfo(ruralPeopleInfo));
    }

    /**
     * 删除人口信息
     */
    @PreAuthorize("@ss.hasPermi('ruralrims:info:remove')")
    @Log(title = "人口信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralPeopleInfoService.deleteRuralPeopleInfoByRIds(rIds));
    }
}
