package com.rural.web.controller.ruralpcm;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralVote;
import com.rural.system.service.IRuralVoteService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 我要投票Controller
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/ruralpcm/vote")
public class RuralVoteController extends BaseController
{
    @Autowired
    private IRuralVoteService ruralVoteService;

    /**
     * 查询我要投票列表
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralVote ruralVote)
    {
        startPage();
        List<RuralVote> list = ruralVoteService.selectRuralVoteList(ruralVote);
        return getDataTable(list);
    }

    /**
     * 导出我要投票列表
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:export')")
    @Log(title = "我要投票", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralVote ruralVote)
    {
        List<RuralVote> list = ruralVoteService.selectRuralVoteList(ruralVote);
        ExcelUtil<RuralVote> util = new ExcelUtil<RuralVote>(RuralVote.class);
        util.exportExcel(response, list, "我要投票数据");
    }

    /**
     * 获取我要投票详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralVoteService.selectRuralVoteByRId(rId));
    }

    /**
     * 新增我要投票
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:add')")
    @Log(title = "我要投票", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralVote ruralVote)
    {
        return toAjax(ruralVoteService.insertRuralVote(ruralVote));
    }

    /**
     * 修改我要投票
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:edit')")
    @Log(title = "我要投票", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralVote ruralVote)
    {
        return toAjax(ruralVoteService.updateRuralVote(ruralVote));
    }

    /**
     * 删除我要投票
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:remove')")
    @Log(title = "我要投票", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralVoteService.deleteRuralVoteByRIds(rIds));
    }

    /**
     * 投票
     */
    @PreAuthorize("@ss.hasPermi('ruralpcm:vote:vote')")
    @Log(title = "投票", businessType = BusinessType.UPDATE)
    @PostMapping("/vote")
    public AjaxResult vote(@RequestBody RuralVote ruralVote)
    {

        return toAjax(ruralVoteService.vote(ruralVote));
    }
}
