package com.rural.web.controller.ruraleconomic;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import com.rural.system.domain.RuralSpecialty;
import com.rural.system.service.IRuralSpecialtyService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 乡村特产Controller
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
@RestController
@RequestMapping("/ruraleconomic/specialty")
public class RuralSpecialtyController extends BaseController
{
    @Autowired
    private IRuralSpecialtyService ruralSpecialtyService;

    /**
     * 查询乡村特产列表
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:specialty:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralSpecialty ruralSpecialty)
    {
        startPage();
        List<RuralSpecialty> list = ruralSpecialtyService.selectRuralSpecialtyList(ruralSpecialty);
        return getDataTable(list);
    }

    /**
     * 导出乡村特产列表
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:specialty:export')")
    @Log(title = "乡村特产", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralSpecialty ruralSpecialty)
    {
        List<RuralSpecialty> list = ruralSpecialtyService.selectRuralSpecialtyList(ruralSpecialty);
        ExcelUtil<RuralSpecialty> util = new ExcelUtil<RuralSpecialty>(RuralSpecialty.class);
        util.exportExcel(response, list, "乡村特产数据");
    }

    /**
     * 获取乡村特产详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:specialty:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralSpecialtyService.selectRuralSpecialtyByRId(rId));
    }

    /**
     * 新增乡村特产
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:specialty:add')")
    @Log(title = "乡村特产", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralSpecialty ruralSpecialty)
    {
        return toAjax(ruralSpecialtyService.insertRuralSpecialty(ruralSpecialty));
    }

    /**
     * 修改乡村特产
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:specialty:edit')")
    @Log(title = "乡村特产", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralSpecialty ruralSpecialty)
    {
        return toAjax(ruralSpecialtyService.updateRuralSpecialty(ruralSpecialty));
    }

    /**
     * 删除乡村特产
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:specialty:remove')")
    @Log(title = "乡村特产", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralSpecialtyService.deleteRuralSpecialtyByRIds(rIds));
    }
}
