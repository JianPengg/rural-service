package com.rural.web.controller.ruraleconomic;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.rural.common.annotation.Log;
import com.rural.common.core.controller.BaseController;
import com.rural.common.core.domain.AjaxResult;
import com.rural.common.enums.BusinessType;
import com.rural.system.domain.RuralBid;
import com.rural.system.service.IRuralBidService;
import com.rural.common.utils.poi.ExcelUtil;
import com.rural.common.core.page.TableDataInfo;

/**
 * 乡村项目招标Controller
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/ruraleconomic/bid")
public class RuralBidController extends BaseController
{
    @Autowired
    private IRuralBidService ruralBidService;

    /**
     * 查询乡村项目招标列表
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:bid:list')")
    @GetMapping("/list")
    public TableDataInfo list(RuralBid ruralBid)
    {
        startPage();
        List<RuralBid> list = ruralBidService.selectRuralBidList(ruralBid);
        return getDataTable(list);
    }

    /**
     * 导出乡村项目招标列表
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:bid:export')")
    @Log(title = "乡村项目招标", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, RuralBid ruralBid)
    {
        List<RuralBid> list = ruralBidService.selectRuralBidList(ruralBid);
        ExcelUtil<RuralBid> util = new ExcelUtil<RuralBid>(RuralBid.class);
        util.exportExcel(response, list, "乡村项目招标数据");
    }

    /**
     * 获取乡村项目招标详细信息
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:bid:query')")
    @GetMapping(value = "/{rId}")
    public AjaxResult getInfo(@PathVariable("rId") Long rId)
    {
        return AjaxResult.success(ruralBidService.selectRuralBidByRId(rId));
    }

    /**
     * 新增乡村项目招标
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:bid:add')")
    @Log(title = "乡村项目招标", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody RuralBid ruralBid)
    {
        return toAjax(ruralBidService.insertRuralBid(ruralBid));
    }

    /**
     * 修改乡村项目招标
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:bid:edit')")
    @Log(title = "乡村项目招标", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody RuralBid ruralBid)
    {
        return toAjax(ruralBidService.updateRuralBid(ruralBid));
    }

    /**
     * 删除乡村项目招标
     */
    @PreAuthorize("@ss.hasPermi('ruraleconomic:bid:remove')")
    @Log(title = "乡村项目招标", businessType = BusinessType.DELETE)
	@DeleteMapping("/{rIds}")
    public AjaxResult remove(@PathVariable Long[] rIds)
    {
        return toAjax(ruralBidService.deleteRuralBidByRIds(rIds));
    }
}
