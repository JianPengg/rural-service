package com.rural.common.constant;


/**
 * @ClassNameTablePrimaryKey
 * @Description 表格主键常量池
 * @Author jiangpeng
 * @Date2022-01-26 11:56
 * @Version V1.0
 **/
public class TablePrimaryKey {

    /**
     * 文件表主键
     */
    public static final String PK_FILE_ID = "pk_file_id";

    /**
     * @description:乡村特产主键
     * @author jiangpeng
     * @date: 2022/5/11 21:17
     */
    public static final String PK_RURAL_SPECIALTY_ID = "pk_rural_specialty_id";

    /**
     * @description:乡村景观主键
     * @author jiangpeng
     * @date: 2022/5/11 21:17
     */
    public static final String PK_RURAL_LANDSCAPE_ID = "pk_rural_landscape_id";

    /**
     * @description: 人口信息主键
     * @author jiangpeng
     * @date: 2022/5/12 20:08
     */
    public static final String PK_RURAL_PEOPLE_INFO_ID = "PK_RURAL_PEOPLE_INFO_ID";

    /**
     * @description: 申请跑腿主键
     * @author jiangpeng
     * @date: 2022/5/13 11:32
     */
    public static final String PK_RURAL_APPLY_RUN_ERRANDS_ID = "pk_rural_apply_run_errands_id";

    /**
     * @description:  维修类型主键
     * @author jiangpeng
     * @date: 2022/5/13 13:20
     */
    public static final String PK_RURAL_APPLY_MAINTAIN_ID = "pk_rural_apply_maintain_id";

    /**
     * @description:  申请基础设施主键
     * @author jiangpeng
     * @date: 2022/5/13 14:23
     */
    public static final String PK_RURAL_APPLY_INFRASTRUCTURE_ID = "pk_rural_apply_infrastructure_id";

    /**
     * 我有建议
     */
    public static final String PK_RURAL_PROPOSAL_ID = "pk_rural_proposal_id";

    /**
     * 我要投诉
     */
    public static final String PK_RURAL_COMPLAINT_ID = "pk_rural_complaint_id";

    /**
     * 我要投票
     */
    public static final String PK_RURAL_VOTE_ID = "pk_rural_vote_id";

    /**
     * 乡村项目招标
     */
    public static final String PK_RURAL_BID_ID = "pk_rural_bid_id";
}
