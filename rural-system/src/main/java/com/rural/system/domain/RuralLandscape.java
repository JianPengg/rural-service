package com.rural.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 乡村景观对象 rural_landscape
 * 
 * @author ruoyi
 * @date 2022-05-10
 */
public class RuralLandscape extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 备注 */
    @Excel(name = "备注")
    private String rRemark;

    /** 标题 */
    @Excel(name = "标题")
    private String rTitle;

    /** 文件id */
    @Excel(name = "文件id")
    private Long fileId;

    /** 文件名称 */
    private String fileName;

    /** 文件地址 */
    private String filePath;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setrId(Long rId)
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrRemark(String rRemark) 
    {
        this.rRemark = rRemark;
    }

    public String getrRemark() 
    {
        return rRemark;
    }
    public void setrTitle(String rTitle) 
    {
        this.rTitle = rTitle;
    }

    public String getrTitle() 
    {
        return rTitle;
    }
    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rRemark", getrRemark())
            .append("rTitle", getrTitle())
            .append("fileId", getFileId())
            .toString();
    }
}
