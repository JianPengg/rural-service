package com.rural.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 乡村项目招标对象 rural_bid
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public class RuralBid extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 招标单位 */
    @Excel(name = "招标单位")
    private String rTenderee;

    /** 招标项目 */
    @Excel(name = "招标项目")
    private String rTender;

    /** 招标内容 */
    @Excel(name = "招标内容")
    private String rBidContent;

    /** 投标人资格要求 */
    @Excel(name = "投标人资格要求")
    private String rQrfb;

    /** 招标时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "招标时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date rBidTime;

    /** 招标地点 */
    @Excel(name = "招标地点")
    private String rBidAddr;

    /** 招标文件售价 */
    @Excel(name = "招标文件售价")
    private BigDecimal rBidPrice;

    /** 截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date rEndTime;

    /** 联系人 */
    @Excel(name = "联系人")
    private String rContacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String rPhone;

    public void setrId(Long rId) 
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrTenderee(String rTenderee) 
    {
        this.rTenderee = rTenderee;
    }

    public String getrTenderee() 
    {
        return rTenderee;
    }
    public void setrTender(String rTender) 
    {
        this.rTender = rTender;
    }

    public String getrTender() 
    {
        return rTender;
    }
    public void setrBidContent(String rBidContent) 
    {
        this.rBidContent = rBidContent;
    }

    public String getrBidContent() 
    {
        return rBidContent;
    }
    public void setrQrfb(String rQrfb) 
    {
        this.rQrfb = rQrfb;
    }

    public String getrQrfb() 
    {
        return rQrfb;
    }
    public void setrBidTime(Date rBidTime) 
    {
        this.rBidTime = rBidTime;
    }

    public Date getrBidTime() 
    {
        return rBidTime;
    }
    public void setrBidAddr(String rBidAddr) 
    {
        this.rBidAddr = rBidAddr;
    }

    public String getrBidAddr() 
    {
        return rBidAddr;
    }
    public void setrBidPrice(BigDecimal rBidPrice) 
    {
        this.rBidPrice = rBidPrice;
    }

    public BigDecimal getrBidPrice() 
    {
        return rBidPrice;
    }
    public void setrEndTime(Date rEndTime) 
    {
        this.rEndTime = rEndTime;
    }

    public Date getrEndTime() 
    {
        return rEndTime;
    }
    public void setrContacts(String rContacts) 
    {
        this.rContacts = rContacts;
    }

    public String getrContacts() 
    {
        return rContacts;
    }
    public void setrPhone(String rPhone) 
    {
        this.rPhone = rPhone;
    }

    public String getrPhone() 
    {
        return rPhone;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rTenderee", getrTenderee())
            .append("rTender", getrTender())
            .append("rBidContent", getrBidContent())
            .append("rQrfb", getrQrfb())
            .append("rBidTime", getrBidTime())
            .append("rBidAddr", getrBidAddr())
            .append("rBidPrice", getrBidPrice())
            .append("rEndTime", getrEndTime())
            .append("rContacts", getrContacts())
            .append("rPhone", getrPhone())
            .toString();
    }
}
