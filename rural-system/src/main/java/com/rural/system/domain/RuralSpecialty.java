package com.rural.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 乡村特产对象 rural_specialty
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
public class RuralSpecialty extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 名称 */
    @Excel(name = "名称")
    private String rName;

    /** 购买状态（0未出售 1已出售） */
    @Excel(name = "购买状态", readConverterExp = "0=未出售,1=已出售")
    private String rStatus;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal rPrice;

    /** 介绍 */
    @Excel(name = "介绍")
    private String rIntroduce;

    /** 文件id */
    private Long fileId;

    /** 文件名称 */
    private String fileName;

    /** 文件地址 */
    private String filePath;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setrId(Long rId)
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrStatus(String rStatus) 
    {
        this.rStatus = rStatus;
    }

    public String getrStatus() 
    {
        return rStatus;
    }
    public void setrPrice(BigDecimal rPrice) 
    {
        this.rPrice = rPrice;
    }

    public BigDecimal getrPrice() 
    {
        return rPrice;
    }
    public void setrIntroduce(String rIntroduce) 
    {
        this.rIntroduce = rIntroduce;
    }

    public String getrIntroduce() 
    {
        return rIntroduce;
    }
    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }
    public void setrName(String rName) 
    {
        this.rName = rName;
    }

    public String getrName() 
    {
        return rName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rStatus", getrStatus())
            .append("rPrice", getrPrice())
            .append("rIntroduce", getrIntroduce())
            .append("fileId", getFileId())
            .append("rName", getrName())
            .toString();
    }
}
