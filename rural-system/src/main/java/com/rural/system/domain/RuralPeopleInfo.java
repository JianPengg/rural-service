package com.rural.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 人口信息对象 rural_people_info
 * 
 * @author ruoyi
 * @date 2022-05-11
 */
public class RuralPeopleInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 姓名 */
    @Excel(name = "姓名")
    private String rName;

    /** 性别（0男 1女） */
    @Excel(name = "性别", readConverterExp = "0=男,1=女")
    private String rSex;

    /** 年龄 */
    @Excel(name = "年龄")
    private Long rAge;

    /** 电话号码 */
    @Excel(name = "电话号码")
    private String rPhone;

    /** 地址 */
    @Excel(name = "地址")
    private String rAddr;

    public void setrId(Long rId) 
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrName(String rName) 
    {
        this.rName = rName;
    }

    public String getrName() 
    {
        return rName;
    }
    public void setrSex(String rSex) 
    {
        this.rSex = rSex;
    }

    public String getrSex() 
    {
        return rSex;
    }
    public void setrAge(Long rAge) 
    {
        this.rAge = rAge;
    }

    public Long getrAge() 
    {
        return rAge;
    }
    public void setrPhone(String rPhone)
    {
        this.rPhone = rPhone;
    }

    public String getrPhone()
    {
        return rPhone;
    }
    public void setrAddr(String rAddr) 
    {
        this.rAddr = rAddr;
    }

    public String getrAddr() 
    {
        return rAddr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rName", getrName())
            .append("rSex", getrSex())
            .append("rAge", getrAge())
            .append("rPhone", getrPhone())
            .append("rAddr", getrAddr())
            .toString();
    }
}
