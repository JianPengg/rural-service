package com.rural.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 我有建议对象 rural_proposal
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
public class RuralProposal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 建议内容 */
    @Excel(name = "建议内容")
    private String rRemark;

    public void setrId(Long rId)
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrRemark(String rRemark) 
    {
        this.rRemark = rRemark;
    }

    public String getrRemark() 
    {
        return rRemark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rRemark", getrRemark())
            .toString();
    }
}
