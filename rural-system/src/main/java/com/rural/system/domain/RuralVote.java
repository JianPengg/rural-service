package com.rural.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 我要投票对象 rural_vote
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public class RuralVote extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 发布投票标题 */
    @Excel(name = "发布投票标题")
    private String rTitle;

    /** 发布投票内容 */
    @Excel(name = "发布投票内容")
    private String rRemark;

    /** 人数 */
    @Excel(name = "人数")
    private Long rNum;

    /** 投票人员 */
    @Excel(name = "投票人员")
    private String rPersonnel;

    /** 是否已经投过票 */
    private Boolean voteFlag;

    public Boolean getVoteFlag() {
        return voteFlag;
    }

    public void setVoteFlag(Boolean voteFlag) {
        this.voteFlag = voteFlag;
    }

    public void setrId(Long rId) 
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrNum(Long rNum) 
    {
        this.rNum = rNum;
    }

    public Long getrNum() 
    {
        return rNum;
    }
    public void setrRemark(String rRemark) 
    {
        this.rRemark = rRemark;
    }

    public String getrRemark() 
    {
        return rRemark;
    }
    public void setrTitle(String rTitle) 
    {
        this.rTitle = rTitle;
    }

    public String getrTitle() 
    {
        return rTitle;
    }
    public void setrPersonnel(String rPersonnel) 
    {
        this.rPersonnel = rPersonnel;
    }

    public String getrPersonnel() 
    {
        return rPersonnel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rNum", getrNum())
            .append("rRemark", getrRemark())
            .append("rTitle", getrTitle())
            .append("rPersonnel", getrPersonnel())
            .toString();
    }
}
