package com.rural.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 我要投诉对象 rural_complaint
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public class RuralComplaint extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 投诉内容 */
    @Excel(name = "投诉内容")
    private String rRemark;

    /** 是否匿名(0不匿名 1匿名) */
    @Excel(name = "是否匿名(0不匿名 1匿名)")
    private String rStatus;

    public void setrId(Long rId) 
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrRemark(String rRemark) 
    {
        this.rRemark = rRemark;
    }

    public String getrRemark() 
    {
        return rRemark;
    }
    public void setrStatus(String rStatus) 
    {
        this.rStatus = rStatus;
    }

    public String getrStatus() 
    {
        return rStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rRemark", getrRemark())
            .append("rStatus", getrStatus())
            .toString();
    }
}
