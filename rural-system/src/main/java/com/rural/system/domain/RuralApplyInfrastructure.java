package com.rural.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.rural.common.annotation.Excel;
import com.rural.common.core.domain.BaseEntity;

/**
 * 申请基础设施对象 rural_apply_infrastructure
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
public class RuralApplyInfrastructure extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long rId;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 设施类型(0乡村基础设施 1公共设施) */
    @Excel(name = "设施类型", readConverterExp = "0=乡村基础设施 ,1=公共设施")
    private String rType;

    /** 备注 */
    @Excel(name = "备注")
    private String rRemark;

    public void setrId(Long rId) 
    {
        this.rId = rId;
    }

    public Long getrId() 
    {
        return rId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setrType(String rType) 
    {
        this.rType = rType;
    }

    public String getrType() 
    {
        return rType;
    }
    public void setrRemark(String rRemark) 
    {
        this.rRemark = rRemark;
    }

    public String getrRemark() 
    {
        return rRemark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("rId", getrId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
            .append("rType", getrType())
            .append("rRemark", getrRemark())
            .toString();
    }
}
