package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralApplyRunErrandsMapper;
import com.rural.system.domain.RuralApplyRunErrands;
import com.rural.system.service.IRuralApplyRunErrandsService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 申请跑腿Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
@Service
public class RuralApplyRunErrandsServiceImpl implements IRuralApplyRunErrandsService 
{
    @Autowired
    private RuralApplyRunErrandsMapper ruralApplyRunErrandsMapper;

    @Autowired
    private RedisService redisService;
    /**
     * 查询申请跑腿
     * 
     * @param rId 申请跑腿主键
     * @return 申请跑腿
     */
    @Override
    public RuralApplyRunErrands selectRuralApplyRunErrandsByRId(Long rId)
    {
        return ruralApplyRunErrandsMapper.selectRuralApplyRunErrandsByRId(rId);
    }

    /**
     * 查询申请跑腿列表
     * 
     * @param ruralApplyRunErrands 申请跑腿
     * @return 申请跑腿
     */
    @Override
    public List<RuralApplyRunErrands> selectRuralApplyRunErrandsList(RuralApplyRunErrands ruralApplyRunErrands)
    {
        return ruralApplyRunErrandsMapper.selectRuralApplyRunErrandsList(ruralApplyRunErrands);
    }

    /**
     * 新增申请跑腿
     * 
     * @param ruralApplyRunErrands 申请跑腿
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralApplyRunErrands(RuralApplyRunErrands ruralApplyRunErrands)
    {
        long ruralApplyRunErrandsId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_APPLY_RUN_ERRANDS_ID, 10000L);
        ruralApplyRunErrands.setrId(ruralApplyRunErrandsId);
        ruralApplyRunErrands.setCreateTime(DateUtils.getNowDate());
        ruralApplyRunErrands.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralApplyRunErrandsMapper.insertRuralApplyRunErrands(ruralApplyRunErrands);
    }

    /**
     * 修改申请跑腿
     * 
     * @param ruralApplyRunErrands 申请跑腿
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralApplyRunErrands(RuralApplyRunErrands ruralApplyRunErrands)
    {
        ruralApplyRunErrands.setUpdateTime(DateUtils.getNowDate());
        ruralApplyRunErrands.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralApplyRunErrandsMapper.updateRuralApplyRunErrands(ruralApplyRunErrands);
    }

    /**
     * 批量删除申请跑腿
     * 
     * @param rIds 需要删除的申请跑腿主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralApplyRunErrandsByRIds(Long[] rIds)
    {
        return ruralApplyRunErrandsMapper.deleteRuralApplyRunErrandsByRIds(rIds);
    }

    /**
     * 删除申请跑腿信息
     * 
     * @param rId 申请跑腿主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralApplyRunErrandsByRId(Long rId)
    {
        return ruralApplyRunErrandsMapper.deleteRuralApplyRunErrandsByRId(rId);
    }
}
