package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.config.RuoYiConfig;
import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import com.rural.common.utils.StringUtils;
import com.rural.system.domain.RuralSpecialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralLandscapeMapper;
import com.rural.system.domain.RuralLandscape;
import com.rural.system.service.IRuralLandscapeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 乡村景观Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-10
 */
@Service
public class RuralLandscapeServiceImpl implements IRuralLandscapeService 
{
    @Autowired
    private RuralLandscapeMapper ruralLandscapeMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询乡村景观
     * 
     * @param rId 乡村景观主键
     * @return 乡村景观
     */
    @Override
    public RuralLandscape selectRuralLandscapeByRId(Long rId)
    {
        RuralLandscape ruralLandscape = ruralLandscapeMapper.selectRuralLandscapeByRId(rId);
        if(StringUtils.isNotEmpty(ruralLandscape.getFilePath())){
            ruralLandscape.setFilePath(RuoYiConfig.getFileurl()+ruralLandscape.getFilePath());
        }
        return ruralLandscape;
    }

    /**
     * 查询乡村景观列表
     * 
     * @param ruralLandscape 乡村景观
     * @return 乡村景观
     */
    @Override
    public List<RuralLandscape> selectRuralLandscapeList(RuralLandscape ruralLandscape)
    {
        List<RuralLandscape> ruralLandscapes = ruralLandscapeMapper.selectRuralLandscapeList(ruralLandscape);
        ruralLandscapes.stream().forEach(e->{
            if(StringUtils.isNotEmpty(e.getFilePath())){
                e.setFilePath(RuoYiConfig.getFileurl()+e.getFilePath());
            }
        });
        return ruralLandscapes;
    }

    /**
     * 新增乡村景观
     * 
     * @param ruralLandscape 乡村景观
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralLandscape(RuralLandscape ruralLandscape)
    {
        long landscapeId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_LANDSCAPE_ID, 10000L);
        ruralLandscape.setCreateTime(DateUtils.getNowDate());
        ruralLandscape.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        ruralLandscape.setrId(landscapeId);
        return ruralLandscapeMapper.insertRuralLandscape(ruralLandscape);
    }

    /**
     * 修改乡村景观
     * 
     * @param ruralLandscape 乡村景观
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralLandscape(RuralLandscape ruralLandscape)
    {
        ruralLandscape.setUpdateTime(DateUtils.getNowDate());
        ruralLandscape.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralLandscapeMapper.updateRuralLandscape(ruralLandscape);
    }

    /**
     * 批量删除乡村景观
     * 
     * @param rIds 需要删除的乡村景观主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralLandscapeByRIds(Long[] rIds)
    {
        return ruralLandscapeMapper.deleteRuralLandscapeByRIds(rIds);
    }

    /**
     * 删除乡村景观信息
     * 
     * @param rId 乡村景观主键
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteRuralLandscapeByRId(Long rId)
    {
        return ruralLandscapeMapper.deleteRuralLandscapeByRId(rId);
    }
}
