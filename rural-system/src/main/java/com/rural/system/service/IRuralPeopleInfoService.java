package com.rural.system.service;

import java.util.HashMap;
import java.util.List;
import com.rural.system.domain.RuralPeopleInfo;

/**
 * 人口信息Service接口
 * 
 * @author ruoyi
 * @date 2022-05-11
 */
public interface IRuralPeopleInfoService 
{
    /** 
     * @description: 查询人口比例 
     * @param: ruralPeopleInfo 
     * @return: java.util.HashMap<java.lang.String,java.lang.Object> 
     * @author jiangpeng
     * @date: 2022/5/13 11:25
     */
    public HashMap<String,Object> getPeopleRatio(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 查询人口信息
     * 
     * @param rId 人口信息主键
     * @return 人口信息
     */
    public RuralPeopleInfo selectRuralPeopleInfoByRId(Long rId);
    /**
     * 查询人口信息列表
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 人口信息集合
     */
    public List<RuralPeopleInfo> selectRuralPeopleInfoList(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 新增人口信息
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 结果
     */
    public int insertRuralPeopleInfo(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 修改人口信息
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 结果
     */
    public int updateRuralPeopleInfo(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 批量删除人口信息
     * 
     * @param rIds 需要删除的人口信息主键集合
     * @return 结果
     */
    public int deleteRuralPeopleInfoByRIds(Long[] rIds);

    /**
     * 删除人口信息信息
     * 
     * @param rId 人口信息主键
     * @return 结果
     */
    public int deleteRuralPeopleInfoByRId(Long rId);
}
