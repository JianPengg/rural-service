package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralBid;

/**
 * 乡村项目招标Service接口
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public interface IRuralBidService 
{
    /**
     * 查询乡村项目招标
     * 
     * @param rId 乡村项目招标主键
     * @return 乡村项目招标
     */
    public RuralBid selectRuralBidByRId(Long rId);

    /**
     * 查询乡村项目招标列表
     * 
     * @param ruralBid 乡村项目招标
     * @return 乡村项目招标集合
     */
    public List<RuralBid> selectRuralBidList(RuralBid ruralBid);

    /**
     * 新增乡村项目招标
     * 
     * @param ruralBid 乡村项目招标
     * @return 结果
     */
    public int insertRuralBid(RuralBid ruralBid);

    /**
     * 修改乡村项目招标
     * 
     * @param ruralBid 乡村项目招标
     * @return 结果
     */
    public int updateRuralBid(RuralBid ruralBid);

    /**
     * 批量删除乡村项目招标
     * 
     * @param rIds 需要删除的乡村项目招标主键集合
     * @return 结果
     */
    public int deleteRuralBidByRIds(Long[] rIds);

    /**
     * 删除乡村项目招标信息
     * 
     * @param rId 乡村项目招标主键
     * @return 结果
     */
    public int deleteRuralBidByRId(Long rId);
}
