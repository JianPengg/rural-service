package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralProposal;

/**
 * 我有建议Service接口
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
public interface IRuralProposalService 
{
    /**
     * 查询我有建议
     * 
     * @param rId 我有建议主键
     * @return 我有建议
     */
    public RuralProposal selectRuralProposalByRId(Long rId);

    /**
     * 查询我有建议列表
     * 
     * @param ruralProposal 我有建议
     * @return 我有建议集合
     */
    public List<RuralProposal> selectRuralProposalList(RuralProposal ruralProposal);

    /**
     * 新增我有建议
     * 
     * @param ruralProposal 我有建议
     * @return 结果
     */
    public int insertRuralProposal(RuralProposal ruralProposal);

    /**
     * 修改我有建议
     * 
     * @param ruralProposal 我有建议
     * @return 结果
     */
    public int updateRuralProposal(RuralProposal ruralProposal);

    /**
     * 批量删除我有建议
     * 
     * @param rIds 需要删除的我有建议主键集合
     * @return 结果
     */
    public int deleteRuralProposalByRIds(Long[] rIds);

    /**
     * 删除我有建议信息
     * 
     * @param rId 我有建议主键
     * @return 结果
     */
    public int deleteRuralProposalByRId(Long rId);
}
