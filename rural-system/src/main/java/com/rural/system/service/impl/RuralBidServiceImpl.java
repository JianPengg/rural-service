package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralBidMapper;
import com.rural.system.domain.RuralBid;
import com.rural.system.service.IRuralBidService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 乡村项目招标Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@Service
public class RuralBidServiceImpl implements IRuralBidService 
{
    @Autowired
    private RuralBidMapper ruralBidMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询乡村项目招标
     * 
     * @param rId 乡村项目招标主键
     * @return 乡村项目招标
     */
    @Override
    public RuralBid selectRuralBidByRId(Long rId)
    {
        return ruralBidMapper.selectRuralBidByRId(rId);
    }

    /**
     * 查询乡村项目招标列表
     * 
     * @param ruralBid 乡村项目招标
     * @return 乡村项目招标
     */
    @Override
    public List<RuralBid> selectRuralBidList(RuralBid ruralBid)
    {
        return ruralBidMapper.selectRuralBidList(ruralBid);
    }

    /**
     * 新增乡村项目招标
     * 
     * @param ruralBid 乡村项目招标
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralBid(RuralBid ruralBid)
    {
        long ruralBidID = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_BID_ID, 10000L);
        ruralBid.setrId(ruralBidID);
        ruralBid.setCreateTime(DateUtils.getNowDate());
        ruralBid.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralBidMapper.insertRuralBid(ruralBid);
    }

    /**
     * 修改乡村项目招标
     * 
     * @param ruralBid 乡村项目招标
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralBid(RuralBid ruralBid)
    {
        ruralBid.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        ruralBid.setUpdateTime(DateUtils.getNowDate());
        return ruralBidMapper.updateRuralBid(ruralBid);
    }

    /**
     * 批量删除乡村项目招标
     * 
     * @param rIds 需要删除的乡村项目招标主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralBidByRIds(Long[] rIds)
    {
        return ruralBidMapper.deleteRuralBidByRIds(rIds);
    }

    /**
     * 删除乡村项目招标信息
     * 
     * @param rId 乡村项目招标主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralBidByRId(Long rId)
    {
        return ruralBidMapper.deleteRuralBidByRId(rId);
    }
}
