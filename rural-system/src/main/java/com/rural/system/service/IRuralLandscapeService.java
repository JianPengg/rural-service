package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralLandscape;

/**
 * 乡村景观Service接口
 * 
 * @author ruoyi
 * @date 2022-05-10
 */
public interface IRuralLandscapeService 
{
    /**
     * 查询乡村景观
     * 
     * @param rId 乡村景观主键
     * @return 乡村景观
     */
    public RuralLandscape selectRuralLandscapeByRId(Long rId);

    /**
     * 查询乡村景观列表
     * 
     * @param ruralLandscape 乡村景观
     * @return 乡村景观集合
     */
    public List<RuralLandscape> selectRuralLandscapeList(RuralLandscape ruralLandscape);

    /**
     * 新增乡村景观
     * 
     * @param ruralLandscape 乡村景观
     * @return 结果
     */
    public int insertRuralLandscape(RuralLandscape ruralLandscape);

    /**
     * 修改乡村景观
     * 
     * @param ruralLandscape 乡村景观
     * @return 结果
     */
    public int updateRuralLandscape(RuralLandscape ruralLandscape);

    /**
     * 批量删除乡村景观
     * 
     * @param rIds 需要删除的乡村景观主键集合
     * @return 结果
     */
    public int deleteRuralLandscapeByRIds(Long[] rIds);

    /**
     * 删除乡村景观信息
     * 
     * @param rId 乡村景观主键
     * @return 结果
     */
    public int deleteRuralLandscapeByRId(Long rId);
}
