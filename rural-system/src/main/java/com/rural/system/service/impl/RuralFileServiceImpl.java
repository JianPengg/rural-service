package com.rural.system.service.impl;

import java.util.List;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralFileMapper;
import com.rural.system.domain.RuralFile;
import com.rural.system.service.IRuralFileService;

/**
 * 文件图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
@Service
public class RuralFileServiceImpl implements IRuralFileService 
{
    @Autowired
    private RuralFileMapper ruralFileMapper;

    /**
     * 查询文件图片
     * 
     * @param id 文件图片主键
     * @return 文件图片
     */
    @Override
    public RuralFile selectRuralFileById(Long id)
    {
        return ruralFileMapper.selectRuralFileById(id);
    }

    /**
     * 查询文件图片列表
     * 
     * @param ruralFile 文件图片
     * @return 文件图片
     */
    @Override
    public List<RuralFile> selectRuralFileList(RuralFile ruralFile)
    {
        return ruralFileMapper.selectRuralFileList(ruralFile);
    }

    /**
     * 新增文件图片
     * 
     * @param ruralFile 文件图片
     * @return 结果
     */
    @Override
    public int insertRuralFile(RuralFile ruralFile)
    {
        ruralFile.setCreateTime(DateUtils.getNowDate());
        return ruralFileMapper.insertRuralFile(ruralFile);
    }

    /**
     * 修改文件图片
     * 
     * @param ruralFile 文件图片
     * @return 结果
     */
    @Override
    public int updateRuralFile(RuralFile ruralFile)
    {
        return ruralFileMapper.updateRuralFile(ruralFile);
    }

    /**
     * 批量删除文件图片
     * 
     * @param ids 需要删除的文件图片主键
     * @return 结果
     */
    @Override
    public int deleteRuralFileByIds(Long[] ids)
    {
        return ruralFileMapper.deleteRuralFileByIds(ids);
    }

    /**
     * 删除文件图片信息
     * 
     * @param id 文件图片主键
     * @return 结果
     */
    @Override
    public int deleteRuralFileById(Long id)
    {
        return ruralFileMapper.deleteRuralFileById(id);
    }
}
