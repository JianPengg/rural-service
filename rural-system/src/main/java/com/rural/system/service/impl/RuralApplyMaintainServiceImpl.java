package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralApplyMaintainMapper;
import com.rural.system.domain.RuralApplyMaintain;
import com.rural.system.service.IRuralApplyMaintainService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 申请维修Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
@Service
public class RuralApplyMaintainServiceImpl implements IRuralApplyMaintainService 
{
    @Autowired
    private RuralApplyMaintainMapper ruralApplyMaintainMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询申请维修
     * 
     * @param rId 申请维修主键
     * @return 申请维修
     */
    @Override
    public RuralApplyMaintain selectRuralApplyMaintainByRId(Long rId)
    {
        return ruralApplyMaintainMapper.selectRuralApplyMaintainByRId(rId);
    }

    /**
     * 查询申请维修列表
     * 
     * @param ruralApplyMaintain 申请维修
     * @return 申请维修
     */
    @Override
    public List<RuralApplyMaintain> selectRuralApplyMaintainList(RuralApplyMaintain ruralApplyMaintain)
    {
        return ruralApplyMaintainMapper.selectRuralApplyMaintainList(ruralApplyMaintain);
    }

    /**
     * 新增申请维修
     * 
     * @param ruralApplyMaintain 申请维修
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralApplyMaintain(RuralApplyMaintain ruralApplyMaintain)
    {
        long ruralApplyMaintainId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_APPLY_MAINTAIN_ID, 10000L);
        ruralApplyMaintain.setrId(ruralApplyMaintainId);
        ruralApplyMaintain.setCreateTime(DateUtils.getNowDate());
        ruralApplyMaintain.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralApplyMaintainMapper.insertRuralApplyMaintain(ruralApplyMaintain);
    }

    /**
     * 修改申请维修
     * 
     * @param ruralApplyMaintain 申请维修
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralApplyMaintain(RuralApplyMaintain ruralApplyMaintain)
    {
        ruralApplyMaintain.setUpdateTime(DateUtils.getNowDate());
        ruralApplyMaintain.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralApplyMaintainMapper.updateRuralApplyMaintain(ruralApplyMaintain);
    }

    /**
     * 批量删除申请维修
     * 
     * @param rIds 需要删除的申请维修主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralApplyMaintainByRIds(Long[] rIds)
    {
        return ruralApplyMaintainMapper.deleteRuralApplyMaintainByRIds(rIds);
    }

    /**
     * 删除申请维修信息
     * 
     * @param rId 申请维修主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralApplyMaintainByRId(Long rId)
    {
        return ruralApplyMaintainMapper.deleteRuralApplyMaintainByRId(rId);
    }
}
