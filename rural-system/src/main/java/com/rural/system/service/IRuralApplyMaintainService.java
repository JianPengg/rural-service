package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralApplyMaintain;

/**
 * 申请维修Service接口
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
public interface IRuralApplyMaintainService 
{
    /**
     * 查询申请维修
     * 
     * @param rId 申请维修主键
     * @return 申请维修
     */
    public RuralApplyMaintain selectRuralApplyMaintainByRId(Long rId);

    /**
     * 查询申请维修列表
     * 
     * @param ruralApplyMaintain 申请维修
     * @return 申请维修集合
     */
    public List<RuralApplyMaintain> selectRuralApplyMaintainList(RuralApplyMaintain ruralApplyMaintain);

    /**
     * 新增申请维修
     * 
     * @param ruralApplyMaintain 申请维修
     * @return 结果
     */
    public int insertRuralApplyMaintain(RuralApplyMaintain ruralApplyMaintain);

    /**
     * 修改申请维修
     * 
     * @param ruralApplyMaintain 申请维修
     * @return 结果
     */
    public int updateRuralApplyMaintain(RuralApplyMaintain ruralApplyMaintain);

    /**
     * 批量删除申请维修
     * 
     * @param rIds 需要删除的申请维修主键集合
     * @return 结果
     */
    public int deleteRuralApplyMaintainByRIds(Long[] rIds);

    /**
     * 删除申请维修信息
     * 
     * @param rId 申请维修主键
     * @return 结果
     */
    public int deleteRuralApplyMaintainByRId(Long rId);
}
