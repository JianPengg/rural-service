package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralFile;

/**
 * 文件图片Service接口
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
public interface IRuralFileService 
{
    /**
     * 查询文件图片
     * 
     * @param id 文件图片主键
     * @return 文件图片
     */
    public RuralFile selectRuralFileById(Long id);

    /**
     * 查询文件图片列表
     * 
     * @param ruralFile 文件图片
     * @return 文件图片集合
     */
    public List<RuralFile> selectRuralFileList(RuralFile ruralFile);

    /**
     * 新增文件图片
     * 
     * @param ruralFile 文件图片
     * @return 结果
     */
    public int insertRuralFile(RuralFile ruralFile);

    /**
     * 修改文件图片
     * 
     * @param ruralFile 文件图片
     * @return 结果
     */
    public int updateRuralFile(RuralFile ruralFile);

    /**
     * 批量删除文件图片
     * 
     * @param ids 需要删除的文件图片主键集合
     * @return 结果
     */
    public int deleteRuralFileByIds(Long[] ids);

    /**
     * 删除文件图片信息
     * 
     * @param id 文件图片主键
     * @return 结果
     */
    public int deleteRuralFileById(Long id);
}
