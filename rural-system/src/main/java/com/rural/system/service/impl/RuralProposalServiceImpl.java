package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralProposalMapper;
import com.rural.system.domain.RuralProposal;
import com.rural.system.service.IRuralProposalService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 我有建议Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-17
 */
@Service
public class RuralProposalServiceImpl implements IRuralProposalService 
{
    @Autowired
    private RuralProposalMapper ruralProposalMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询我有建议
     * 
     * @param rId 我有建议主键
     * @return 我有建议
     */
    @Override
    public RuralProposal selectRuralProposalByRId(Long rId)
    {
        return ruralProposalMapper.selectRuralProposalByRId(rId);
    }

    /**
     * 查询我有建议列表
     * 
     * @param ruralProposal 我有建议
     * @return 我有建议
     */
    @Override
    public List<RuralProposal> selectRuralProposalList(RuralProposal ruralProposal)
    {
        return ruralProposalMapper.selectRuralProposalList(ruralProposal);
    }

    /**
     * 新增我有建议
     * 
     * @param ruralProposal 我有建议
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralProposal(RuralProposal ruralProposal)
    {
        long ruralProposalId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_PROPOSAL_ID, 10000L);
        ruralProposal.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        ruralProposal.setrId(ruralProposalId);
        ruralProposal.setCreateTime(DateUtils.getNowDate());
        return ruralProposalMapper.insertRuralProposal(ruralProposal);
    }

    /**
     * 修改我有建议
     * 
     * @param ruralProposal 我有建议
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralProposal(RuralProposal ruralProposal)
    {
        ruralProposal.setUpdateTime(DateUtils.getNowDate());
        ruralProposal.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralProposalMapper.updateRuralProposal(ruralProposal);
    }

    /**
     * 批量删除我有建议
     * 
     * @param rIds 需要删除的我有建议主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralProposalByRIds(Long[] rIds)
    {
        return ruralProposalMapper.deleteRuralProposalByRIds(rIds);
    }

    /**
     * 删除我有建议信息
     * 
     * @param rId 我有建议主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralProposalByRId(Long rId)
    {
        return ruralProposalMapper.deleteRuralProposalByRId(rId);
    }
}
