package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralApplyInfrastructure;

/**
 * 申请基础设施Service接口
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
public interface IRuralApplyInfrastructureService 
{
    /**
     * 查询申请基础设施
     * 
     * @param rId 申请基础设施主键
     * @return 申请基础设施
     */
    public RuralApplyInfrastructure selectRuralApplyInfrastructureByRId(Long rId);

    /**
     * 查询申请基础设施列表
     * 
     * @param ruralApplyInfrastructure 申请基础设施
     * @return 申请基础设施集合
     */
    public List<RuralApplyInfrastructure> selectRuralApplyInfrastructureList(RuralApplyInfrastructure ruralApplyInfrastructure);

    /**
     * 新增申请基础设施
     * 
     * @param ruralApplyInfrastructure 申请基础设施
     * @return 结果
     */
    public int insertRuralApplyInfrastructure(RuralApplyInfrastructure ruralApplyInfrastructure);

    /**
     * 修改申请基础设施
     * 
     * @param ruralApplyInfrastructure 申请基础设施
     * @return 结果
     */
    public int updateRuralApplyInfrastructure(RuralApplyInfrastructure ruralApplyInfrastructure);

    /**
     * 批量删除申请基础设施
     * 
     * @param rIds 需要删除的申请基础设施主键集合
     * @return 结果
     */
    public int deleteRuralApplyInfrastructureByRIds(Long[] rIds);

    /**
     * 删除申请基础设施信息
     * 
     * @param rId 申请基础设施主键
     * @return 结果
     */
    public int deleteRuralApplyInfrastructureByRId(Long rId);
}
