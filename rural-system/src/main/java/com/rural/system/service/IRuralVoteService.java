package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralVote;

/**
 * 我要投票Service接口
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public interface IRuralVoteService 
{
    /**
     * 查询我要投票
     * 
     * @param rId 我要投票主键
     * @return 我要投票
     */
    public RuralVote selectRuralVoteByRId(Long rId);

    /**
     * 查询我要投票列表
     * 
     * @param ruralVote 我要投票
     * @return 我要投票集合
     */
    public List<RuralVote> selectRuralVoteList(RuralVote ruralVote);

    /**
     * 新增我要投票
     * 
     * @param ruralVote 我要投票
     * @return 结果
     */
    public int insertRuralVote(RuralVote ruralVote);

    /**
     * 修改我要投票
     * 
     * @param ruralVote 我要投票
     * @return 结果
     */
    public int updateRuralVote(RuralVote ruralVote);

    /**
     * 批量删除我要投票
     * 
     * @param rIds 需要删除的我要投票主键集合
     * @return 结果
     */
    public int deleteRuralVoteByRIds(Long[] rIds);

    /**
     * 删除我要投票信息
     * 
     * @param rId 我要投票主键
     * @return 结果
     */
    public int deleteRuralVoteByRId(Long rId);

    /**
     * 投票
     *
     * @param ruralVote 投票
     * @return 结果
     */
    public int vote(RuralVote ruralVote);
}
