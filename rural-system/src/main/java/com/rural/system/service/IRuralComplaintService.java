package com.rural.system.service;

import java.util.List;
import com.rural.system.domain.RuralComplaint;

/**
 * 我要投诉Service接口
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public interface IRuralComplaintService 
{
    /**
     * 查询我要投诉
     * 
     * @param rId 我要投诉主键
     * @return 我要投诉
     */
    public RuralComplaint selectRuralComplaintByRId(Long rId);

    /**
     * 查询我要投诉列表
     * 
     * @param ruralComplaint 我要投诉
     * @return 我要投诉集合
     */
    public List<RuralComplaint> selectRuralComplaintList(RuralComplaint ruralComplaint);

    /**
     * 新增我要投诉
     * 
     * @param ruralComplaint 我要投诉
     * @return 结果
     */
    public int insertRuralComplaint(RuralComplaint ruralComplaint);

    /**
     * 修改我要投诉
     * 
     * @param ruralComplaint 我要投诉
     * @return 结果
     */
    public int updateRuralComplaint(RuralComplaint ruralComplaint);

    /**
     * 批量删除我要投诉
     * 
     * @param rIds 需要删除的我要投诉主键集合
     * @return 结果
     */
    public int deleteRuralComplaintByRIds(Long[] rIds);

    /**
     * 删除我要投诉信息
     * 
     * @param rId 我要投诉主键
     * @return 结果
     */
    public int deleteRuralComplaintByRId(Long rId);
}
