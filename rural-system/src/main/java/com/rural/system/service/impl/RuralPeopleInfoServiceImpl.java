package com.rural.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralPeopleInfoMapper;
import com.rural.system.domain.RuralPeopleInfo;
import com.rural.system.service.IRuralPeopleInfoService;
import org.springframework.transaction.annotation.Transactional;

import javax.security.auth.callback.Callback;

/**
 * 人口信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-11
 */
@Service
public class RuralPeopleInfoServiceImpl implements IRuralPeopleInfoService 
{
    @Autowired
    private RuralPeopleInfoMapper ruralPeopleInfoMapper;

    @Autowired
    private RedisService redisService;

    /** 
     * @description: 查询人口比例 
     * @param: ruralPeopleInfo 
     * @return: java.util.HashMap<java.lang.String,java.lang.Object> 
     * @author jiangpeng
     * @date: 2022/5/12 22:15
     */
    @Override
    public HashMap<String, Object> getPeopleRatio(RuralPeopleInfo ruralPeopleInfo) {

        HashMap<String,Object> resMap = new HashMap<>();

        //查询男女比例
        List<HashMap<String, Object>> mfList = ruralPeopleInfoMapper.getMf(ruralPeopleInfo);
        //性别list
        List<String> mfList1 = new ArrayList<>();
        mfList.stream().forEach(e->{
            mfList1.add(e.get("name")+"");
        });
        HashMap<String,Object> dataMfMap = new HashMap<>();
        dataMfMap.put("legendData",mfList1);
        dataMfMap.put("seriesData",mfList);

        resMap.put("dataMf",dataMfMap);


        //查询小孩，成年，老人比例
        //小于18小孩，大于等于18并且小于60成年,大于60老人
        List<Integer> caopList = ruralPeopleInfoMapper.getCaop(ruralPeopleInfo);
        //小孩，成年，老人list
        List<String> caopList1 = Arrays.asList("小孩","成年","老人");
        List<HashMap<String,Object>> caopListMap = new ArrayList<>();
        for (int i = 0; i < caopList.size(); i++) {
            HashMap<String,Object> caopMap = new HashMap<>();
            caopMap.put("name",caopList1.get(i));
            caopMap.put("value",caopList.get(i));
            caopListMap.add(caopMap);
        }
        HashMap<String,Object> dataCaopMap = new HashMap<>();
        dataCaopMap.put("legendData",caopList1);
        dataCaopMap.put("seriesData",caopListMap);

        resMap.put("dataCaop",dataCaopMap);


        return resMap;
    }

    /**
     * 查询人口信息
     * 
     * @param rId 人口信息主键
     * @return 人口信息
     */
    @Override
    public RuralPeopleInfo selectRuralPeopleInfoByRId(Long rId)
    {
        return ruralPeopleInfoMapper.selectRuralPeopleInfoByRId(rId);
    }

    /**
     * 查询人口信息列表
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 人口信息
     */
    @Override
    public List<RuralPeopleInfo> selectRuralPeopleInfoList(RuralPeopleInfo ruralPeopleInfo)
    {
        return ruralPeopleInfoMapper.selectRuralPeopleInfoList(ruralPeopleInfo);
    }

    /**
     * 新增人口信息
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralPeopleInfo(RuralPeopleInfo ruralPeopleInfo)
    {
        long ruralPepoleInfoId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_PEOPLE_INFO_ID, 10000L);
        ruralPeopleInfo.setrId(ruralPepoleInfoId);
        ruralPeopleInfo.setCreateTime(DateUtils.getNowDate());
        ruralPeopleInfo.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralPeopleInfoMapper.insertRuralPeopleInfo(ruralPeopleInfo);
    }

    /**
     * 修改人口信息
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralPeopleInfo(RuralPeopleInfo ruralPeopleInfo)
    {
        ruralPeopleInfo.setUpdateTime(DateUtils.getNowDate());
        ruralPeopleInfo.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralPeopleInfoMapper.updateRuralPeopleInfo(ruralPeopleInfo);
    }

    /**
     * 批量删除人口信息
     * 
     * @param rIds 需要删除的人口信息主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralPeopleInfoByRIds(Long[] rIds)
    {
        return ruralPeopleInfoMapper.deleteRuralPeopleInfoByRIds(rIds);
    }

    /**
     * 删除人口信息信息
     * 
     * @param rId 人口信息主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralPeopleInfoByRId(Long rId)
    {
        return ruralPeopleInfoMapper.deleteRuralPeopleInfoByRId(rId);
    }
}
