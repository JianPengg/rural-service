package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.config.RuoYiConfig;
import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import com.rural.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralSpecialtyMapper;
import com.rural.system.domain.RuralSpecialty;
import com.rural.system.service.IRuralSpecialtyService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 乡村特产Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
@Service
public class RuralSpecialtyServiceImpl implements IRuralSpecialtyService 
{
    @Autowired
    private RuralSpecialtyMapper ruralSpecialtyMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询乡村特产
     * 
     * @param rId 乡村特产主键
     * @return 乡村特产
     */
    @Override
    public RuralSpecialty selectRuralSpecialtyByRId(Long rId)
    {
        RuralSpecialty ruralSpecialty = ruralSpecialtyMapper.selectRuralSpecialtyByRId(rId);
        if(StringUtils.isNotEmpty(ruralSpecialty.getFilePath())){
            ruralSpecialty.setFilePath(RuoYiConfig.getFileurl()+ruralSpecialty.getFilePath());
        }
        return ruralSpecialty;
    }

    /**
     * 查询乡村特产列表
     * 
     * @param ruralSpecialty 乡村特产
     * @return 乡村特产
     */
    @Override
    public List<RuralSpecialty> selectRuralSpecialtyList(RuralSpecialty ruralSpecialty)
    {
        List<RuralSpecialty> ruralSpecialties = ruralSpecialtyMapper.selectRuralSpecialtyList(ruralSpecialty);
        ruralSpecialties.stream().forEach(e->{
            if(StringUtils.isNotEmpty(e.getFilePath())){
                e.setFilePath(RuoYiConfig.getFileurl()+e.getFilePath());
            }
        });
        return ruralSpecialties;
    }

    /**
     * 新增乡村特产
     * 
     * @param ruralSpecialty 乡村特产
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralSpecialty(RuralSpecialty ruralSpecialty)
    {
        long ruralSpecialtyId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_LANDSCAPE_ID, 10000L);
        ruralSpecialty.setCreateTime(DateUtils.getNowDate());
        ruralSpecialty.setrId(ruralSpecialtyId);
        ruralSpecialty.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralSpecialtyMapper.insertRuralSpecialty(ruralSpecialty);
    }

    /**
     * 修改乡村特产
     * 
     * @param ruralSpecialty 乡村特产
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralSpecialty(RuralSpecialty ruralSpecialty)
    {
        ruralSpecialty.setUpdateTime(DateUtils.getNowDate());
        ruralSpecialty.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralSpecialtyMapper.updateRuralSpecialty(ruralSpecialty);
    }

    /**
     * 批量删除乡村特产
     * 
     * @param rIds 需要删除的乡村特产主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralSpecialtyByRIds(Long[] rIds)
    {
        return ruralSpecialtyMapper.deleteRuralSpecialtyByRIds(rIds);
    }

    /**
     * 删除乡村特产信息
     * 
     * @param rId 乡村特产主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralSpecialtyByRId(Long rId)
    {
        return ruralSpecialtyMapper.deleteRuralSpecialtyByRId(rId);
    }
}
