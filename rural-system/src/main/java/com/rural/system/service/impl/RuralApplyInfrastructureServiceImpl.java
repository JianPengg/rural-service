package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralApplyInfrastructureMapper;
import com.rural.system.domain.RuralApplyInfrastructure;
import com.rural.system.service.IRuralApplyInfrastructureService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 申请基础设施Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
@Service
public class RuralApplyInfrastructureServiceImpl implements IRuralApplyInfrastructureService 
{
    @Autowired
    private RuralApplyInfrastructureMapper ruralApplyInfrastructureMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询申请基础设施
     * 
     * @param rId 申请基础设施主键
     * @return 申请基础设施
     */
    @Override
    public RuralApplyInfrastructure selectRuralApplyInfrastructureByRId(Long rId)
    {
        return ruralApplyInfrastructureMapper.selectRuralApplyInfrastructureByRId(rId);
    }

    /**
     * 查询申请基础设施列表
     * 
     * @param ruralApplyInfrastructure 申请基础设施
     * @return 申请基础设施
     */
    @Override
    public List<RuralApplyInfrastructure> selectRuralApplyInfrastructureList(RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        return ruralApplyInfrastructureMapper.selectRuralApplyInfrastructureList(ruralApplyInfrastructure);
    }

    /**
     * 新增申请基础设施
     * 
     * @param ruralApplyInfrastructure 申请基础设施
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralApplyInfrastructure(RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        long ruralApplyInfrastructureId = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_APPLY_INFRASTRUCTURE_ID, 10000L);
        ruralApplyInfrastructure.setrId(ruralApplyInfrastructureId);
        ruralApplyInfrastructure.setCreateTime(DateUtils.getNowDate());
        ruralApplyInfrastructure.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralApplyInfrastructureMapper.insertRuralApplyInfrastructure(ruralApplyInfrastructure);
    }

    /**
     * 修改申请基础设施
     * 
     * @param ruralApplyInfrastructure 申请基础设施
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralApplyInfrastructure(RuralApplyInfrastructure ruralApplyInfrastructure)
    {
        ruralApplyInfrastructure.setUpdateTime(DateUtils.getNowDate());
        ruralApplyInfrastructure.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        return ruralApplyInfrastructureMapper.updateRuralApplyInfrastructure(ruralApplyInfrastructure);
    }

    /**
     * 批量删除申请基础设施
     * 
     * @param rIds 需要删除的申请基础设施主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralApplyInfrastructureByRIds(Long[] rIds)
    {
        return ruralApplyInfrastructureMapper.deleteRuralApplyInfrastructureByRIds(rIds);
    }

    /**
     * 删除申请基础设施信息
     * 
     * @param rId 申请基础设施主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralApplyInfrastructureByRId(Long rId)
    {
        return ruralApplyInfrastructureMapper.deleteRuralApplyInfrastructureByRId(rId);
    }
}
