package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralComplaintMapper;
import com.rural.system.domain.RuralComplaint;
import com.rural.system.service.IRuralComplaintService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 我要投诉Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@Service
public class RuralComplaintServiceImpl implements IRuralComplaintService 
{
    @Autowired
    private RuralComplaintMapper ruralComplaintMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询我要投诉
     * 
     * @param rId 我要投诉主键
     * @return 我要投诉
     */
    @Override
    public RuralComplaint selectRuralComplaintByRId(Long rId)
    {
        return ruralComplaintMapper.selectRuralComplaintByRId(rId);
    }

    /**
     * 查询我要投诉列表
     * 
     * @param ruralComplaint 我要投诉
     * @return 我要投诉
     */
    @Override
    public List<RuralComplaint> selectRuralComplaintList(RuralComplaint ruralComplaint)
    {
        return ruralComplaintMapper.selectRuralComplaintList(ruralComplaint);
    }

    /**
     * 新增我要投诉
     * 
     * @param ruralComplaint 我要投诉
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralComplaint(RuralComplaint ruralComplaint)
    {
        long ruralComplaintID = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_COMPLAINT_ID, 10000L);
        ruralComplaint.setrId(ruralComplaintID);
        ruralComplaint.setCreateTime(DateUtils.getNowDate());
        if("1".equals(ruralComplaint.getrStatus())){
            ruralComplaint.setCreateBy("匿名");
        }else{
            ruralComplaint.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        }
        return ruralComplaintMapper.insertRuralComplaint(ruralComplaint);
    }

    /**
     * 修改我要投诉
     * 
     * @param ruralComplaint 我要投诉
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralComplaint(RuralComplaint ruralComplaint)
    {
        if("1".equals(ruralComplaint.getrStatus())){
            ruralComplaint.setCreateBy("匿名");
        }else{
            ruralComplaint.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        }
        ruralComplaint.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        ruralComplaint.setUpdateTime(DateUtils.getNowDate());
        return ruralComplaintMapper.updateRuralComplaint(ruralComplaint);
    }

    /**
     * 批量删除我要投诉
     * 
     * @param rIds 需要删除的我要投诉主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralComplaintByRIds(Long[] rIds)
    {
        return ruralComplaintMapper.deleteRuralComplaintByRIds(rIds);
    }

    /**
     * 删除我要投诉信息
     * 
     * @param rId 我要投诉主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralComplaintByRId(Long rId)
    {
        return ruralComplaintMapper.deleteRuralComplaintByRId(rId);
    }
}
