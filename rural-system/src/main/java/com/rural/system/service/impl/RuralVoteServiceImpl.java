package com.rural.system.service.impl;

import java.util.List;

import com.rural.common.constant.TablePrimaryKey;
import com.rural.common.service.RedisService;
import com.rural.common.utils.DateUtils;
import com.rural.common.utils.SecurityUtils;
import com.rural.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rural.system.mapper.RuralVoteMapper;
import com.rural.system.domain.RuralVote;
import com.rural.system.service.IRuralVoteService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 我要投票Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@Service
public class RuralVoteServiceImpl implements IRuralVoteService 
{
    @Autowired
    private RuralVoteMapper ruralVoteMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询我要投票
     * 
     * @param rId 我要投票主键
     * @return 我要投票
     */
    @Override
    public RuralVote selectRuralVoteByRId(Long rId)
    {
        return ruralVoteMapper.selectRuralVoteByRId(rId);
    }

    /**
     * 查询我要投票列表
     * 
     * @param ruralVote 我要投票
     * @return 我要投票
     */
    @Override
    public List<RuralVote> selectRuralVoteList(RuralVote ruralVote)
    {
        String userName = SecurityUtils.getLoginUser().getUsername();
        //查询我要投票列表
        List<RuralVote> ruralVoteList = ruralVoteMapper.selectRuralVoteList(ruralVote);
        ruralVoteList.stream().forEach(e->{
            if(e.getrPersonnel().contains(userName)){
                e.setVoteFlag(true);
            }
        });
        return ruralVoteList;
    }

    /**
     * 新增我要投票
     * 
     * @param ruralVote 我要投票
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertRuralVote(RuralVote ruralVote)
    {
        long ruralVoteID = redisService.getAutoIncrementID(TablePrimaryKey.PK_RURAL_VOTE_ID, 10000L);
//        ruralVote.setrPersonnel(SecurityUtils.getLoginUser().getUsername());
        ruralVote.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        ruralVote.setrId(ruralVoteID);
        ruralVote.setCreateTime(DateUtils.getNowDate());
        return ruralVoteMapper.insertRuralVote(ruralVote);
    }

    /**
     * 修改我要投票
     * 
     * @param ruralVote 我要投票
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRuralVote(RuralVote ruralVote)
    {
        ruralVote.setUpdateBy(SecurityUtils.getLoginUser().getUsername());
        ruralVote.setUpdateTime(DateUtils.getNowDate());
        return ruralVoteMapper.updateRuralVote(ruralVote);
    }

    /**
     * 批量删除我要投票
     * 
     * @param rIds 需要删除的我要投票主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralVoteByRIds(Long[] rIds)
    {
        return ruralVoteMapper.deleteRuralVoteByRIds(rIds);
    }

    /**
     * 删除我要投票信息
     * 
     * @param rId 我要投票主键
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteRuralVoteByRId(Long rId)
    {
        return ruralVoteMapper.deleteRuralVoteByRId(rId);
    }


    /**
     * 投票
     *
     * @param ruralVote 投票
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int vote(RuralVote ruralVote) {

        //登陆用户
        String username = SecurityUtils.getLoginUser().getUsername();

        //查询我要投票
        RuralVote ruralVote1 = ruralVoteMapper.selectRuralVoteByRId(ruralVote.getrId());

        //投票人数加1
        ruralVote1.setrNum(ruralVote1.getrNum()+1);

        //如果不是第一次投票那么就拼接投票人员
        if(StringUtils.isNotEmpty(ruralVote1.getrPersonnel())){
            ruralVote1.setrPersonnel(ruralVote1.getrPersonnel()+","+username);
        }else{
            ruralVote1.setrPersonnel(username);
        }
        ruralVote1.setUpdateBy(username);
        ruralVote1.setUpdateTime(DateUtils.getNowDate());

        return ruralVoteMapper.updateRuralVote(ruralVote1);
    }
}
