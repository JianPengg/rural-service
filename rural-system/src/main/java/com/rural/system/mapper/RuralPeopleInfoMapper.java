package com.rural.system.mapper;

import java.util.HashMap;
import java.util.List;
import com.rural.system.domain.RuralPeopleInfo;

/**
 * 人口信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-11
 */
public interface RuralPeopleInfoMapper 
{
    /**
     * 查询人口信息
     * 
     * @param rId 人口信息主键
     * @return 人口信息
     */
    public RuralPeopleInfo selectRuralPeopleInfoByRId(Long rId);

    /**
     * 查询人口信息列表
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 人口信息集合
     */
    public List<RuralPeopleInfo> selectRuralPeopleInfoList(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 新增人口信息
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 结果
     */
    public int insertRuralPeopleInfo(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 修改人口信息
     * 
     * @param ruralPeopleInfo 人口信息
     * @return 结果
     */
    public int updateRuralPeopleInfo(RuralPeopleInfo ruralPeopleInfo);

    /**
     * 删除人口信息
     * 
     * @param rId 人口信息主键
     * @return 结果
     */
    public int deleteRuralPeopleInfoByRId(Long rId);

    /**
     * 批量删除人口信息
     * 
     * @param rIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRuralPeopleInfoByRIds(Long[] rIds);

    /**
     * @description: 查询男女比例
     * @param: ruralPeopleInfo
     * @return: java.util.List<java.util.HashMap<java.lang.String,java.lang.Object>>
     * @author jiangpeng
     * @date: 2022/5/12 22:40
     */
    public List<HashMap<String,Object>> getMf(RuralPeopleInfo ruralPeopleInfo);

    /**
     * @description:  查询小孩，成年，老人比例
     * 小于18小孩，大于等于18并且小于60成年,大于60老人
     * @param: ruralPeopleInfo
     * @return: java.util.List<java.lang.Integer>
     * @author jiangpeng
     * @date: 2022/5/12 22:40
     */
    public List<Integer> getCaop(RuralPeopleInfo ruralPeopleInfo);
}
