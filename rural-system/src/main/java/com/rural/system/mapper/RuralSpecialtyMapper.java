package com.rural.system.mapper;

import java.util.List;
import com.rural.system.domain.RuralSpecialty;

/**
 * 乡村特产Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
public interface RuralSpecialtyMapper 
{
    /**
     * 查询乡村特产
     * 
     * @param rId 乡村特产主键
     * @return 乡村特产
     */
    public RuralSpecialty selectRuralSpecialtyByRId(Long rId);

    /**
     * 查询乡村特产列表
     * 
     * @param ruralSpecialty 乡村特产
     * @return 乡村特产集合
     */
    public List<RuralSpecialty> selectRuralSpecialtyList(RuralSpecialty ruralSpecialty);

    /**
     * 新增乡村特产
     * 
     * @param ruralSpecialty 乡村特产
     * @return 结果
     */
    public int insertRuralSpecialty(RuralSpecialty ruralSpecialty);

    /**
     * 修改乡村特产
     * 
     * @param ruralSpecialty 乡村特产
     * @return 结果
     */
    public int updateRuralSpecialty(RuralSpecialty ruralSpecialty);

    /**
     * 删除乡村特产
     * 
     * @param rId 乡村特产主键
     * @return 结果
     */
    public int deleteRuralSpecialtyByRId(Long rId);

    /**
     * 批量删除乡村特产
     * 
     * @param rIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRuralSpecialtyByRIds(Long[] rIds);
}
