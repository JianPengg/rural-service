package com.rural.system.mapper;

import java.util.List;
import com.rural.system.domain.RuralApplyRunErrands;

/**
 * 申请跑腿Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-13
 */
public interface RuralApplyRunErrandsMapper 
{
    /**
     * 查询申请跑腿
     * 
     * @param rId 申请跑腿主键
     * @return 申请跑腿
     */
    public RuralApplyRunErrands selectRuralApplyRunErrandsByRId(Long rId);

    /**
     * 查询申请跑腿列表
     * 
     * @param ruralApplyRunErrands 申请跑腿
     * @return 申请跑腿集合
     */
    public List<RuralApplyRunErrands> selectRuralApplyRunErrandsList(RuralApplyRunErrands ruralApplyRunErrands);

    /**
     * 新增申请跑腿
     * 
     * @param ruralApplyRunErrands 申请跑腿
     * @return 结果
     */
    public int insertRuralApplyRunErrands(RuralApplyRunErrands ruralApplyRunErrands);

    /**
     * 修改申请跑腿
     * 
     * @param ruralApplyRunErrands 申请跑腿
     * @return 结果
     */
    public int updateRuralApplyRunErrands(RuralApplyRunErrands ruralApplyRunErrands);

    /**
     * 删除申请跑腿
     * 
     * @param rId 申请跑腿主键
     * @return 结果
     */
    public int deleteRuralApplyRunErrandsByRId(Long rId);

    /**
     * 批量删除申请跑腿
     * 
     * @param rIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteRuralApplyRunErrandsByRIds(Long[] rIds);
}
