import request from '@/utils/request'

// 查询乡村项目招标列表
export function listBid(query) {
  return request({
    url: '/ruraleconomic/bid/list',
    method: 'get',
    params: query
  })
}

// 查询乡村项目招标详细
export function getBid(rId) {
  return request({
    url: '/ruraleconomic/bid/' + rId,
    method: 'get'
  })
}

// 新增乡村项目招标
export function addBid(data) {
  return request({
    url: '/ruraleconomic/bid',
    method: 'post',
    data: data
  })
}

// 修改乡村项目招标
export function updateBid(data) {
  return request({
    url: '/ruraleconomic/bid',
    method: 'put',
    data: data
  })
}

// 删除乡村项目招标
export function delBid(rId) {
  return request({
    url: '/ruraleconomic/bid/' + rId,
    method: 'delete'
  })
}
