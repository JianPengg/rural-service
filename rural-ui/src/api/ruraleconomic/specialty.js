import request from '@/utils/request'

// 查询乡村特产列表
export function listSpecialty(query) {
  return request({
    url: '/ruraleconomic/specialty/list',
    method: 'get',
    params: query
  })
}

// 查询乡村特产详细
export function getSpecialty(rId) {
  return request({
    url: '/ruraleconomic/specialty/' + rId,
    method: 'get'
  })
}

// 新增乡村特产
export function addSpecialty(data) {
  return request({
    url: '/ruraleconomic/specialty',
    method: 'post',
    data: data
  })
}

// 修改乡村特产
export function updateSpecialty(data) {
  return request({
    url: '/ruraleconomic/specialty',
    method: 'put',
    data: data
  })
}

// 删除乡村特产
export function delSpecialty(rId) {
  return request({
    url: '/ruraleconomic/specialty/' + rId,
    method: 'delete'
  })
}
