import request from '@/utils/request'

// 查询乡村景观列表
export function listLandscape(query) {
  return request({
    url: '/ruraleconomic/landscape/list',
    method: 'get',
    params: query
  })
}

// 查询乡村景观详细
export function getLandscape(rId) {
  return request({
    url: '/ruraleconomic/landscape/' + rId,
    method: 'get'
  })
}

// 新增乡村景观
export function addLandscape(data) {
  return request({
    url: '/ruraleconomic/landscape',
    method: 'post',
    data: data
  })
}

// 修改乡村景观
export function updateLandscape(data) {
  return request({
    url: '/ruraleconomic/landscape',
    method: 'put',
    data: data
  })
}

// 删除乡村景观
export function delLandscape(rId) {
  return request({
    url: '/ruraleconomic/landscape/' + rId,
    method: 'delete'
  })
}
