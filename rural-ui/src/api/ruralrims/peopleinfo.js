import request from '@/utils/request'

// 查询人口信息列表
export function listInfo(query) {
  return request({
    url: '/ruralrims/peopleinfo/list',
    method: 'get',
    params: query
  })
}

// 查询人口男女比例,小孩,成年,老人比例
export function getPeopleRatio(query) {
  return request({
    url: '/ruralrims/peopleinfo/getPeopleRatio',
    method: 'get',
    params: query
  })
}

// 查询人口信息详细
export function getInfo(rId) {
  return request({
    url: '/ruralrims/peopleinfo/' + rId,
    method: 'get'
  })
}

// 新增人口信息
export function addInfo(data) {
  return request({
    url: '/ruralrims/peopleinfo',
    method: 'post',
    data: data
  })
}

// 修改人口信息
export function updateInfo(data) {
  return request({
    url: '/ruralrims/peopleinfo',
    method: 'put',
    data: data
  })
}

// 删除人口信息
export function delInfo(rId) {
  return request({
    url: '/ruralrims/peopleinfo/' + rId,
    method: 'delete'
  })
}
