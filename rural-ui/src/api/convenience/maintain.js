import request from '@/utils/request'

// 查询申请维修列表
export function listMaintain(query) {
  return request({
    url: '/convenience/maintain/list',
    method: 'get',
    params: query
  })
}

// 查询申请维修详细
export function getMaintain(rId) {
  return request({
    url: '/convenience/maintain/' + rId,
    method: 'get'
  })
}

// 新增申请维修
export function addMaintain(data) {
  return request({
    url: '/convenience/maintain',
    method: 'post',
    data: data
  })
}

// 修改申请维修
export function updateMaintain(data) {
  return request({
    url: '/convenience/maintain',
    method: 'put',
    data: data
  })
}

// 删除申请维修
export function delMaintain(rId) {
  return request({
    url: '/convenience/maintain/' + rId,
    method: 'delete'
  })
}
