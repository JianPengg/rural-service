import request from '@/utils/request'

// 查询申请基础设施列表
export function listInfrastructure(query) {
  return request({
    url: '/convenience/infrastructure/list',
    method: 'get',
    params: query
  })
}

// 查询申请基础设施详细
export function getInfrastructure(rId) {
  return request({
    url: '/convenience/infrastructure/' + rId,
    method: 'get'
  })
}

// 新增申请基础设施
export function addInfrastructure(data) {
  return request({
    url: '/convenience/infrastructure',
    method: 'post',
    data: data
  })
}

// 修改申请基础设施
export function updateInfrastructure(data) {
  return request({
    url: '/convenience/infrastructure',
    method: 'put',
    data: data
  })
}

// 删除申请基础设施
export function delInfrastructure(rId) {
  return request({
    url: '/convenience/infrastructure/' + rId,
    method: 'delete'
  })
}
