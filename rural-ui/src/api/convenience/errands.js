import request from '@/utils/request'

// 查询申请跑腿列表
export function listErrands(query) {
  return request({
    url: '/convenience/errands/list',
    method: 'get',
    params: query
  })
}

// 查询申请跑腿详细
export function getErrands(rId) {
  return request({
    url: '/convenience/errands/' + rId,
    method: 'get'
  })
}

// 新增申请跑腿
export function addErrands(data) {
  return request({
    url: '/convenience/errands',
    method: 'post',
    data: data
  })
}

// 修改申请跑腿
export function updateErrands(data) {
  return request({
    url: '/convenience/errands',
    method: 'put',
    data: data
  })
}

// 删除申请跑腿
export function delErrands(rId) {
  return request({
    url: '/convenience/errands/' + rId,
    method: 'delete'
  })
}
