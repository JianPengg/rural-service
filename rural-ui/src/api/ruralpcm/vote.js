import request from '@/utils/request'

// 查询我要投票列表
export function listVote(query) {
  return request({
    url: '/ruralpcm/vote/list',
    method: 'get',
    params: query
  })
}

// 查询我要投票详细
export function getVote(rId) {
  return request({
    url: '/ruralpcm/vote/' + rId,
    method: 'get'
  })
}

// 新增我要投票
export function addVote(data) {
  return request({
    url: '/ruralpcm/vote',
    method: 'post',
    data: data
  })
}

// 修改我要投票
export function updateVote(data) {
  return request({
    url: '/ruralpcm/vote',
    method: 'put',
    data: data
  })
}

// 删除我要投票
export function delVote(rId) {
  return request({
    url: '/ruralpcm/vote/' + rId,
    method: 'delete'
  })
}

// 投票
export function vote(data) {
  return request({
    url: '/ruralpcm/vote/vote',
    method: 'post',
    data: data
  })
}
