import request from '@/utils/request'

// 查询我要投诉列表
export function listComplaint(query) {
  return request({
    url: '/ruralpcm/complaint/list',
    method: 'get',
    params: query
  })
}

// 查询我要投诉详细
export function getComplaint(rId) {
  return request({
    url: '/ruralpcm/complaint/' + rId,
    method: 'get'
  })
}

// 新增我要投诉
export function addComplaint(data) {
  return request({
    url: '/ruralpcm/complaint',
    method: 'post',
    data: data
  })
}

// 修改我要投诉
export function updateComplaint(data) {
  return request({
    url: '/ruralpcm/complaint',
    method: 'put',
    data: data
  })
}

// 删除我要投诉
export function delComplaint(rId) {
  return request({
    url: '/ruralpcm/complaint/' + rId,
    method: 'delete'
  })
}
