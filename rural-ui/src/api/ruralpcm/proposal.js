import request from '@/utils/request'

// 查询我有建议列表
export function listProposal(query) {
  return request({
    url: '/ruralpcm/proposal/list',
    method: 'get',
    params: query
  })
}

// 查询我有建议详细
export function getProposal(rId) {
  return request({
    url: '/ruralpcm/proposal/' + rId,
    method: 'get'
  })
}

// 新增我有建议
export function addProposal(data) {
  return request({
    url: '/ruralpcm/proposal',
    method: 'post',
    data: data
  })
}

// 修改我有建议
export function updateProposal(data) {
  return request({
    url: '/ruralpcm/proposal',
    method: 'put',
    data: data
  })
}

// 删除我有建议
export function delProposal(rId) {
  return request({
    url: '/ruralpcm/proposal/' + rId,
    method: 'delete'
  })
}
