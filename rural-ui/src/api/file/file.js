import request from '@/utils/request'

// 查询文件图片列表
export function listFile(query) {
  return request({
    url: '/common/list',
    method: 'get',
    params: query
  })
}

// 查询文件图片详细
export function getFile(id) {
  return request({
    url: '/common/' + id,
    method: 'get'
  })
}

// 新增文件图片
export function addFile(data) {
  return request({
    url: '/common',
    method: 'post',
    data: data
  })
}

// 修改文件图片
export function updateFile(data) {
  return request({
    url: '/common',
    method: 'put',
    data: data
  })
}

// 删除文件图片
export function delFile(id) {
  return request({
    url: '/common/' + id,
    method: 'delete'
  })
}
